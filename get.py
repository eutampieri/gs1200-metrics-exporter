#!/usr/bin/env python3
from utils import *
from json import dumps
from sys import argv

ip = argv[1]

token = get_token(ip, argv[2])
# with open("out.json", 'w') as f:
#    print(token)
#    dump(export_metrics(ip, token), f)
metrics = export_metrics(ip, token)
ports = [{
    "up": x == "Up",
    "speed": int(metrics["link"]["speed"][i].split(' ')[0]),
    "rx": metrics["link"]["Stats"][i][6] + metrics["link"]["Stats"][i][7] + metrics["link"]["Stats"][i][8] + metrics["link"]["Stats"][i][10],
    "tx": metrics["link"]["Stats"][i][1] + metrics["link"]["Stats"][i][2] + metrics["link"]["Stats"][i][3],
    "loop": metrics["system"]["loop_status"][i] != "Normal",
}
    for i, x in enumerate(metrics["link"]["portstatus"])]
print(dumps({"ports": ports, "uptime": int(
    metrics["system"]["system_uptime"])}))
logout(ip, token)
