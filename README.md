# GS1200 exporter

This program (`get.py`) exports port metrics from the ZyXEL GS1200 series of switches.

Run it as `./get.py ip password` and you'll get this output:
```json
{
  "ports": [
    {
      "up": true,
      "speed": 100,
      "rx": 6039190,
      "tx": 9721623,
      "loop": false
    }
  ],
  "uptime": 255939
}
```
where `ports` is an array with one item per port, and `uptime` is the uptime in seconds.

**Warning**: TX and RX are packets, not bytes!
