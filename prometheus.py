#!/usr/bin/env python3
import http.server
from utils import *
from json import dumps
from sys import argv


class JSONHTTPExporter(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, 'OK')
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        ip = argv[1]

        token = get_token(ip, argv[2])
        metrics = export_metrics(ip, token)
        ports = [{
            "up": x == "Up",
            "speed": int(metrics["link"]["speed"][i].split(' ')[0]),
            "rx": metrics["link"]["Stats"][i][6] + metrics["link"]["Stats"][i][7] + metrics["link"]["Stats"][i][8] + metrics["link"]["Stats"][i][10],
            "tx": metrics["link"]["Stats"][i][1] + metrics["link"]["Stats"][i][2] + metrics["link"]["Stats"][i][3],
            "loop": metrics["system"]["loop_status"][i] != "Normal",
        }
            for i, x in enumerate(metrics["link"]["portstatus"])]

        self.wfile.write(
            f"# HELP gs1200_uptime\n# TYPE gs1200_uptime gauge\ngs1200_uptime {metrics['system']['system_uptime']}\n".encode("utf-8"))
        self.wfile.write(
            b"# HELP gs1200_port_up If the port is up or not.\n# TYPE gs1200_port_up gauge\n")
        self.wfile.write(
            b"# HELP gs1200_port_speed Link speed in Mbps.\n# TYPE gs1200_port_speed gauge\n")
        self.wfile.write(
            b"# HELP gs1200_rx Incoming packets.\n# TYPE gs1200_rx gauge\n")
        self.wfile.write(
            b"# HELP gs1200_tx Outgoing packets.\n# TYPE gs1200_tx gauge\n")
        self.wfile.write(
            b"# HELP gs1200_port_loop Whether a loop has been detected.\n# TYPE gs1200_port_loop gauge\n")
        for i, port in enumerate(ports):
            self.wfile.write(
                f"gs1200_port_up {{port=\"{i+1}\"}} {int(port['up'])}\n".encode("utf-8"))
            self.wfile.write(
                f"gs1200_port_speed {{port=\"{i+1}\"}} {port['speed']}\n".encode("utf-8"))
            self.wfile.write(
                f"gs1200_port_rx {{port=\"{i+1}\"}} {port['rx']}\n".encode("utf-8"))
            self.wfile.write(
                f"gs1200_port_tx {{port=\"{i+1}\"}} {port['tx']}\n".encode("utf-8"))
            self.wfile.write(
                f"gs1200_port_loop {{port=\"{i+1}\"}} {int(port['loop'])}\n".encode("utf-8"))
        logout(ip, token)


server_address = ('127.0.0.1', 50000)
httpd = http.server.ThreadingHTTPServer(server_address, JSONHTTPExporter)
httpd.serve_forever()
