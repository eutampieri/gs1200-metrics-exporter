from random import randint
import urllib.request
import urllib.parse
from json import loads


def random_string():
    randomStrArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
                    ]
    return randomStrArr[randint(0, len(randomStrArr) - 1)]


def encrypt_string(string):
    un_final_str = ""
    for i in range(0, len(string) + 1):
        if i == len(string):
            un_final_str += random_string()
            break
        else:
            code = ord(string[i])
            temp_str = chr(code - len(string))
            un_final_str += (random_string() + temp_str)
    return un_final_str

# login url: POST http://192.168.1.156/login.cgi
# application/x-www-form-urlencoded password=encrypt_string(pwd)
# token cookie


def get_token(ip, password):
    request = urllib.request.Request(
        f'http://{ip}/login.cgi', data=f"password={urllib.parse.quote_plus(encrypt_string(password))}".encode('ascii'), headers={"Content-Type": "application/x-www-form-urlencoded"}, method="POST")
    response = urllib.request.urlopen(request)
    return response.headers["set-cookie"]


def request_with_token(url, token):
    request = urllib.request.Request(url, headers={"Cookie": token})
    return urllib.request.urlopen(request)


def logout(ip, token):
    request_with_token(f'http://{ip}/logout.html', token)


def parse_js(body):
    rows = [row.replace("var", '') for row in body.strip("\n\r").split(";")]
    identifiers_with_values = [
        [piece.strip("\n\r\t ") for piece in row.split('=')] for row in rows]
    return {piece[0]: loads(piece[1].replace("'", '"').replace(", ", ",").replace(",]", "]")) for piece in identifiers_with_values if len(piece) == 2}


def export_metric(ip, token, metric):
    return parse_js(request_with_token(f"http://{ip}/{metric}_data.js", token).read().decode("ascii"))


def export_metrics(ip, token):
    metrics = ["link", "Advanced_Setting", "VLAN_1Q_List", "VLAN_1Q_New", "port_state",
               "LAG", "mirror", "qos", "igmp", "rate", "loop", "manage_vid", "system"]
    return {metric: export_metric(ip, token, metric) for metric in metrics}


# http://192.168.1.156/frestore.cgi
# multipart

# POST http://192.168.1.156/reboot.cgi
